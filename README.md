# Scripts Ansible para configuração de ambiente e deploy de microserviços
Este projeto contém scripts do ansible para configuração dos ambientes e todas as suas dependências

# Organização
Dentro da pasta ansible temos uma estrutura de pastas e arquivos padronizadas para execução dos scripts.

* ansible/ - pasta com os arquivos de scripts
* filename.yml - arquivo contendo a receita para instalação de determinado recurso em um ambiente (serviço, jenkins, banco de dados, etc)
* run.sh - Comando que aplica o script do ansible dentro do servidor
* dev - pasta contendo arquivos com os hosts e chaves de acesso para o ambiente de desenvolvimento
* qa  - pasta contendo arquivos com os hosts e chaves de acesso para o ambiente de qa
* hml - pasta contendo arquivos com os hosts e chaves de acesso para o ambiente de homologação
* ppd - pasta contendo arquivos com os hosts e chaves de acesso para o ambiente de pré produção
* prd - pasta contendo arquivos com os hosts e chaves de acesso para o ambiente de produção

# Execução
Para aplicar determinado script o comando executado deve ser executado o shell script passando determiandos parâmetros.
O comando obedece a sintaxe **sh run.sh $1 $2 $3 $4** onde:

* $1    - o nome do arquivo yml com as definições de scripts (obrigatório)
* $2    - o ambiente que será executado o script (obrigatório)
* $3    - o caminho para a chave privada de acesso ao ambiente
* $4    - os parâmetros que o script espera para execução em formato json (obrigatório pelo menos o contexto de execução)

## Exemplo de execução sem parâmetros adicionais
sh run.sh jenkins hml /folder/ssh.key '{"context":"aws"}'

## Exemplo de execução com parâmetros
sh run.sh service hml /folder/ssh.key '{"context":"aws","service":"ciee-service-auth","image":"ciee-service-auth:latest","container_port":"8080","env":"dev"}'

# Execução local
Para testes, pode ser necessário a execução local dos scripts ansible. Neste repositório está disponibilizado um Vagrantfile que contém a configuração de uma máquina Ubuntu com docker que pode ser usada para testes.

## Executando o vagrant
Com o vagrant instalado na máquina (https://www.vagrantup.com/downloads.html) navegue até a pasta do arquivo Vagrantfile e execute o comando **vagrant up**. Este comando inicializará a máquina virtual e criará um arquivo dentro da estrutura .vagrant/machines/wordpress/virtualbox/private_key. Copie o conteúdo do arquivo **private_key** para o arquivo **ansible/local/key.pem**.

## Executando o script
Com a chave de acesso copiada, execute o comando para executar o ansible **sh run.sh service local '{"context":"aws","service":"ciee-service-example","image":"keiros/ciee-service-poc-build:latest","container_port":"8080","env":"dev"}'**. Este comando é um exemplo de um container e deve ser alterado para a realidade do teste. Os parâmetros estão explicados abaixo:

* context   - o cluster que o comando será executado de acordo com o arquivo hosts
* service   - o nome do microserviço
* image     - a imagem docker presente na máquina que executa o ansible e que será transportada
* container_port - a porta que o microserviço é executado
* env - o perfil spring cloud utilizado para carregar as configurações do microserviço